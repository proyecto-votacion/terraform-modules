terraform {
  # backend "local"{} 
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 4.0"
    }
  }
  cloud {
     organization = "appvotacion"
    workspaces {
      name = "appvotacion-state-saver"
    } 
  }
}
# variable "aws_region" {
    # type = string
    # default = "us-east-2"
# }
# 
# variable "aws_access_key_id" {
    # type = string
    # default = ""
# }
# 
# variable "aws_secret_access_key" {
    # type = string
    # default = ""
# }
# 
# variable "aws_session_token" {
    # type = string
    # default = ""
# }  


# Configure the AWS Provider
provider "aws" {
  region = "us-east-1"
  # profile = "default"
  # access_key = var.aws_access_key_id
  # secret_key = var.aws_secret_access_key
  # token = var.aws_session_token
  shared_credentials_files = ["~/.aws/credentials"]
}


module "web_api" {
  source = "./web-api/"
}

output "web_api_web_url" {
  value = module.web_api.web_url
}

output "web_api_api_url" {
  value = module.web_api.api_url
}

module "crearKong"{
  source = "./crearKong/"
  web_url = module.web_api.web_url
  api_url = module.web_api.api_url
  depends_on = [module.web_api]
}

module "kong"{
  source = "./kong/"
  kong_load_balancer_security_group = module.web_api.kong_load_balancer_security_group
  kong_service_security_group = module.web_api.kong_service_security_group
  depends_on = [module.crearKong]
}

output "kong_url" {
  value = module.kong.kong_url
}