variable "web_url" {
  description = "URL de la web"
}

variable "api_url" {
  description = "URL de la api"
}

resource "local_file" "kong_yml" {
  filename = "./crearKong/kong.yml"
  content  = <<YAML
_format_version: "1.1"

services:
  - name: web
    url: http://${var.web_url}:5000/
    routes:
      - name: web
        paths:
          - /
        # plugins:
        #   - name: file-log
        #     config:
        #       path: /tmp/logs_web.log

  - name: api
    url: http://${var.api_url}:5001/v1/
    routes:
      - name: api
        paths:
          - /api/v1/
        plugins:
          - name: key-auth
            config:
              hide_credentials: true
          #- name: file-log
            #config:
              #path: /tmp/logs_api.log

        

consumers:
  - username: admin
    keyauth_credentials:
      - key: admin1
YAML
}