resource "aws_default_vpc" "default_vpc" {
}

resource "aws_default_subnet" "default_subnet_a" {
  availability_zone = "us-east-1a"
}

resource "aws_default_subnet" "default_subnet_b" {
  availability_zone = "us-east-1b"
}

resource "aws_ecs_cluster" "cluster_api" {
  name = "cluster_api" # Name your cluster here
}

resource "aws_ecs_task_definition" "api_task" {
  family                   = "api_task" 
  container_definitions    = <<DEFINITION
  [
    {
      "name": "api_task",
      "image": "207265377270.dkr.ecr.us-east-1.amazonaws.com/appvotacion:api", 
      "essential": true,
      "portMappings": [
        {
          "containerPort": 5001,
          "hostPort": 5001
        }
      ],
      "memory": 512,
      "cpu": 256,
      "environment": [
        {
          "name": "HOST",
          "value": "0.0.0.0"
        },
        {
          "name": "PORT",
          "value": "5001"
        },
        {
          "name": "POSTGRES_DB_DOMAIN",
          "value": "test-db.cmmbp63ls758.us-east-1.rds.amazonaws.com"
        },
        {
          "name": "POSTGRES_DB_PORT",
          "value": "5432"
        },
        {
          "name": "POSTGRES_DB_USERNAME",
          "value": "postgres"
        },
        {
          "name": "POSTGRES_DB_PASS",
          "value": "postgres"
        },
        {
          "name": "POSTGRES_DB_NAME",
          "value": "db-vote"
        },
        {
          "name": "SERVER",
          "value": "gevent"
        },
        {
          "name": "LOG_LEVEL",
          "value": "info"
        }
      ]
    }
  ]
  DEFINITION
  requires_compatibilities = ["FARGATE"] # use Fargate as the launch type
  network_mode             = "awsvpc"    # add the AWS VPN network mode as this is required for Fargate
  memory                   = 512         # Specify the memory the container requires
  cpu                      = 256         # Specify the CPU the container requires
  execution_role_arn       = "arn:aws:iam::207265377270:role/LabRole"  #cambiar segun la persona
}

resource "aws_alb" "api_load_balancer" {
  name               = "api-load-balancer" #load balancer name
  load_balancer_type = "application"
  subnets = [ # Referencing the default subnets
    "${aws_default_subnet.default_subnet_a.id}",
    "${aws_default_subnet.default_subnet_b.id}"
  ]
  security_groups = ["${aws_security_group.api_load_balancer_security_group.id}"]
}

resource "aws_security_group" "api_load_balancer_security_group" {
  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
    from_port   = 5000
    to_port     = 5001
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
    from_port   = 8000
    to_port     = 8000
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
   from_port   = 8000
    to_port     = 8000
    protocol    = "tcp"
    security_groups = ["${aws_security_group.kong_service_security_group.id}"]
  }

}

resource "aws_lb_target_group" "api_target_group" {
  name        = "target-group"
  port        = 5001
  protocol    = "HTTP"
  target_type = "ip"
  vpc_id      = "${aws_default_vpc.default_vpc.id}" # default VPC
}

resource "aws_lb_listener" "listener_api" {
  load_balancer_arn = aws_alb.api_load_balancer.arn
  port              = 5001
  protocol          = "HTTP"
  default_action {
    type             = "forward"
    target_group_arn = aws_lb_target_group.api_target_group.arn
  }
}

resource "aws_ecs_service" "api_service" {
  name            = "api-service"     # Name the service
  cluster         = "${aws_ecs_cluster.cluster_api.id}"   # Reference the created Cluster
  task_definition = "${aws_ecs_task_definition.api_task.arn}" # Reference the task that the service will spin up
  launch_type     = "FARGATE"
  desired_count   = 3 # Set up the number of containers to 3

  load_balancer {
    target_group_arn = "${aws_lb_target_group.api_target_group.arn}" # Reference the target group
    container_name   = "${aws_ecs_task_definition.api_task.family}"
    container_port   = 5001 # Specify the container port
  }

  network_configuration {
    subnets          = ["${aws_default_subnet.default_subnet_a.id}", "${aws_default_subnet.default_subnet_b.id}"]
    assign_public_ip = true     # Provide the containers with public IPs
    security_groups  = ["${aws_security_group.api_load_balancer_security_group.id}"]
  }
}

resource "aws_security_group" "api_service_security_group" {
  vpc_id      = "${aws_default_vpc.default_vpc.id}"
  ingress {
    from_port = 0
    to_port   = 0
    protocol  = "-1"
    # Only allowing traffic in from the load balancer security group
    security_groups = ["${aws_security_group.api_load_balancer_security_group.id}"]
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

}

output "api_url" {
  value = aws_alb.api_load_balancer.dns_name
}

resource "aws_ecs_cluster" "web_cluster" {
  name = "web_cluster" # Name your cluster here
  depends_on = [aws_ecs_service.api_service]
}

resource "aws_ecs_task_definition" "web_task" {
  family                   = "web-task" 
  container_definitions    = <<DEFINITION
  [
    {
      "name": "web-task",
      "image": "207265377270.dkr.ecr.us-east-1.amazonaws.com/appvotacion:web", 
      "essential": true,
      "portMappings": [
        {
          "containerPort": 5000,
          "hostPort": 5000
        }
      ],
      "memory": 512,
      "cpu": 256,
      "environment": [
        {
          "name": "BASE_URL",
          "value": "http://${aws_alb.api_load_balancer.dns_name}:5001"
        },
        {
          "name": "HOST",
          "value": "0.0.0.0"
        },
        {
          "name": "PORT",
          "value": "5000"
        }
      ]
    }
  ]
  DEFINITION
  requires_compatibilities = ["FARGATE"] # use Fargate as the launch type
  network_mode             = "awsvpc"    # add the AWS VPN network mode as this is required for Fargate
  memory                   = 512         # Specify the memory the container requires
  cpu                      = 256         # Specify the CPU the container requires
  execution_role_arn       = "arn:aws:iam::207265377270:role/LabRole"  #cambiar segun la persona
}

resource "aws_alb" "web_load_balancer" {
  name               = "web-load-balancer" #load balancer name
  load_balancer_type = "application"
  subnets = [ # Referencing the default subnets
    "${aws_default_subnet.default_subnet_a.id}",
    "${aws_default_subnet.default_subnet_b.id}"
  ]
  security_groups = ["${aws_security_group.web_load_balancer_security_group.id}"]
  depends_on = [aws_ecs_cluster.web_cluster]
}

resource "aws_security_group" "web_load_balancer_security_group" {
  ingress {
    from_port   = 5000
    to_port     = 5000
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"] # Allow traffic in from all sources
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
    from_port   = 8000
    to_port     = 8000
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
   from_port   = 8000
    to_port     = 8000
    protocol    = "tcp"
    security_groups = ["${aws_security_group.kong_service_security_group.id}"]
  }

}

resource "aws_lb_target_group" "web_target_group" {
  name        = "web-target-group"
  port        = 5000
  protocol    = "HTTP"
  target_type = "ip"
  vpc_id      = "${aws_default_vpc.default_vpc.id}" # default VPC
}

resource "aws_lb_listener" "listener_web" {
  load_balancer_arn = "${aws_alb.web_load_balancer.arn}" #  load balancer
  port              = "5000"
  protocol          = "HTTP"
  default_action {
    type             = "forward"
    target_group_arn = "${aws_lb_target_group.web_target_group.arn}" # target group
  }
}

resource "aws_ecs_service" "web_service" {
  name            = "web_service"     # Name the service
  cluster         = "${aws_ecs_cluster.web_cluster.id}"   # Reference the created Cluster
  task_definition = "${aws_ecs_task_definition.web_task.arn}" # Reference the task that the service will spin up
  launch_type     = "FARGATE"
  desired_count   = 3 # Set up the number of containers to 3

  load_balancer {
    target_group_arn = "${aws_lb_target_group.web_target_group.arn}" # Reference the target group
    container_name   = "${aws_ecs_task_definition.web_task.family}"
    container_port   = 5000 # Specify the container port
  }

  network_configuration {
    subnets          = ["${aws_default_subnet.default_subnet_a.id}", "${aws_default_subnet.default_subnet_b.id}"]
    assign_public_ip = true     # Provide the containers with public IPs
    security_groups  = ["${aws_security_group.web_service_security_group.id}"]
  }
  depends_on = [aws_alb.web_load_balancer]
}

resource "aws_security_group" "web_service_security_group" {
  vpc_id      = "${aws_default_vpc.default_vpc.id}"
  ingress {
    from_port = 0
    to_port   = 0
    protocol  = "-1"
    # Only allowing traffic in from the load balancer security group
    security_groups = ["${aws_security_group.web_load_balancer_security_group.id}"]
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
}

output "web_url" {
  value = aws_alb.web_load_balancer.dns_name
}

resource "aws_security_group" "kong_service_security_group" {
  vpc_id      = "${aws_default_vpc.default_vpc.id}"
  ingress {
    from_port = 0
    to_port   = 0
    protocol  = "-1"
    # Only allowing traffic in from the load balancer security group
    security_groups = ["${aws_security_group.kong_load_balancer_security_group.id}"]
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
}

output "kong_service_security_group"{
  value = aws_security_group.kong_service_security_group.id
}

resource "aws_security_group" "kong_load_balancer_security_group" {
  ingress {
    from_port   = 8000
    to_port     = 8000
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"] # Allow traffic in from all sources
  }
  
  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

}

output "kong_load_balancer_security_group"{
  value = aws_security_group.kong_load_balancer_security_group.id
}