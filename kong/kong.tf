variable "kong_load_balancer_security_group" {
  description = "kong load balancer security group id"
}

variable "kong_service_security_group" {
  description = "kong sercice security group id"
}

resource "aws_default_vpc" "default_vpc" {
}

resource "aws_default_subnet" "default_subnet_a" {
  availability_zone = "us-east-1a"
}

resource "aws_default_subnet" "default_subnet_b" {
  availability_zone = "us-east-1b"
}

resource "aws_ecs_cluster" "kong_cluster" {
  name = "kong_cluster" # Name your cluster here
}

resource "aws_ecs_task_definition" "kong_task" {
  family                   = "kong-task"
  container_definitions = jsonencode([
    {
      "name": "kong-task",
      "image": "207265377270.dkr.ecr.us-east-1.amazonaws.com/appvotacion:kong",
      "essential": true,
      "portMappings": [
        {
          "containerPort": 8000,
          "hostPort": 8000
        }
      ],
      "memory": 512,
      "cpu": 256,
      "environment": [
        {
          "name": "KONG_DATABASE",
          "value": "off"
        },
        {
          "name": "KONG_DECLARATIVE_CONFIG"
          "value": "/kong.yml"
        }
      ]
    }
  ])

  requires_compatibilities = ["FARGATE"]
  network_mode             = "awsvpc"
  memory                   = 512
  cpu                      = 256
  execution_role_arn       = "arn:aws:iam::207265377270:role/LabRole"
}

resource "aws_alb" "kong_load_balancer" {
  name               = "kong-load-balancer" #load balancer name
  load_balancer_type = "application"
  subnets = [ # Referencing the default subnets
    "${aws_default_subnet.default_subnet_a.id}",
    "${aws_default_subnet.default_subnet_b.id}"
  ]
  security_groups = ["${var.kong_load_balancer_security_group}"]
  depends_on = [aws_ecs_cluster.kong_cluster]
}

resource "aws_lb_target_group" "kong_target_group" {
  name        = "kong-target-group"
  port        = 8000
  protocol    = "HTTP"
  target_type = "ip"
  vpc_id      = "${aws_default_vpc.default_vpc.id}" # default VPC
}

resource "aws_lb_listener" "listener_kong" {
  load_balancer_arn = "${aws_alb.kong_load_balancer.arn}" #  load balancer
  port              = "8000"
  protocol          = "HTTP"
  default_action {
    type             = "forward"
    target_group_arn = "${aws_lb_target_group.kong_target_group.arn}" # target group
  }
}

resource "aws_ecs_service" "kong_service" {
  name            = "kong_service"     # Name the service
  cluster         = "${aws_ecs_cluster.kong_cluster.id}"   # Reference the created Cluster
  task_definition = "${aws_ecs_task_definition.kong_task.arn}" # Reference the task that the service will spin up
  launch_type     = "FARGATE"
  desired_count   = 3 # Set up the number of containers to 3

  load_balancer {
    target_group_arn = "${aws_lb_target_group.kong_target_group.arn}" # Reference the target group
    container_name   = "${aws_ecs_task_definition.kong_task.family}"
    container_port   = 8000 # Specify the container port
  }

  network_configuration {
    subnets          = ["${aws_default_subnet.default_subnet_a.id}", "${aws_default_subnet.default_subnet_b.id}"]
    assign_public_ip = true     # Provide the containers with public IPs
    security_groups  = ["${var.kong_service_security_group}"]
  }
  depends_on = [aws_alb.kong_load_balancer]
}

output "kong_url" {
  value = aws_alb.kong_load_balancer.dns_name
}