variable "aws_region" {
    type = string
    default = "us-east-2"
}

variable "aws_access_key_id" {
    type = string
    default = ""
}

variable "aws_secret_access_key" {
    type = string
    default = ""
}

variable "aws_session_token" {
    type = string
    default = ""
}  