# terraform-modules

## Modulos

El terrafom tenemos dos archivos. 
El archivo configuracion_ terraform.tf crea la configuración de de terraform, obtiene las credenciales de aws y crea los modulos de terraform.

En la carpeta web-api hay un archivo web_api que crea la infraestructura de web y api en AWS mediante una crecion de un servicio de contenedor que corre en AWS Fargate y con un balanceador de carga. Además, incluye un clúster de ECS, un grupo de destino del balanceador de carga, obtiene el vpc y las 2 subredes, grupos de seguridad y configura todos ellos. Finalmente saca la URL del balanceador de carga por salida.

En la carpeta crearKong hay un archivo crearKong que obtine la salida del modulo de crear web-api y crea un archivo de configuración de rutas de kong para esas rutas.

En la carpeta kong hay un archivo kong que crea la infraestructura de kong en AWS mediante una crecion de un servicio de contenedor que corre en AWS Fargate y con un balanceador de carga. Además, incluye un clúster de ECS, un grupo de destino del balanceador de carga, obtiene el vpc y las 2 subredes, grupos de seguridad y configura todos ellos. Finalmente saca la URL del balanceador de carga por salida.

## Ejecute

Para ejecutar disponemos de los siguientes comandos:
```
Terraform init
terraform apply -target=module.web_api
terraform apply -target=module.crearKong
docker build -t kong:1.0 -f Dockerfile .
./subirContenedor.sh
terraform apply -target=module.kong
terraform destroy
```

Terraform init inicializa, descargando los plugins y creando el fichero de estado. Este comando solo hay que hacerlo la primera vez.
Terraform apply -target= Crea los recursos del modulo target. Si se ha ejecutado anteriormente solo se ejecuta los archivos que cambian.
Docker build crea la imagen de docker de kong
subirContenedor sube la imagen de docker a aws
Terraform destroy destruye todo lo que el apply ha construido.